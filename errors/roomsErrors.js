"use strict"

const emptyTitle = {success: false, error: "title field is empty"}
const vkIdIsIncorrect = {
  success: false,
  error: "vk id is incorrect"
}

const incorrectObjectId = {
  success: false,
  error: 'incorrect objectId'
}

module.exports.emptyTitle = emptyTitle
module.exports.vkIdIsIncorrect = vkIdIsIncorrect
module.exports.incorrectObjectId = incorrectObjectId