"use strict"
var express = require('express');
var router = express.Router();
const Room = require('../models/Room')
const User = require('../models/User')
const Song = require('../models/Song')
const createUser = require('../helpers/userHelper')
const roomsMiddlewares = require('../middlewares/roomsMiddlewares')
/* GET users listing. */
router.get('/rooms', function(req, res, next) {
  Room.find({}, 'title owner').populate('owner', 'vkId firstName lastName')
  .exec()
  .then(function (data) {
    res.json({rooms: data})
  }, (error) => {
    res.json({answer: error})
  })
});

router.post('/rooms',
  roomsMiddlewares.checkRoomTitle,
  roomsMiddlewares.checkVkId,
  function (req, res, next) {
    const userId = req.body.userId
    const newRoom = new Room({
      title: req.body.title,
      owner: userId,
      participations: [userId]
    })
    newRoom.save().then((room)=>{
      console.log(room)
    })
    res.json(newRoom)
})

router.post('/rooms/:id/songs',
  roomsMiddlewares.checkObjectId,
  (req, res, next) => {
    Room.findOne({_id: req.params.id})
    .populate('participations', 'vkId firstName lastName')
    .populate('songs')
    .then((room) => {
      const song = new Song({
        title: req.body.title,
        artist: req.body.artist,
        duration: req.body.duration,
        vkId: req.body.vkId,
        ownerVkId: req.body.ownerVkId
      })
      song.save().then((song)=> {
        room.songs.push(song)
        room.save().then((doc)=>{
          res.send(room)
        })
      })
    })
    console.log(req.body)
  })

router.get('/rooms/:id',
  roomsMiddlewares.checkObjectId,
  (req, res, next) => {
  Room.findOne({_id: req.params.id})
    .populate('owner', 'vkId firstName lastName')
    .populate('songs')
    .populate('participations', 'vkId firstName lastName')
    .then((room) => {
      res.json(room)
    }, (err)=> {
      console.log(err)
    })
})

router.post('/login', function(req, res, next) {
  console.log(req.body)
  let user = {
    id: req.body.id,
    firstName: req.body.firstName,
    lastName: req.body.lastName
  }
  createUser(user).then((user) => {
    console.log(user)
    res.json(user)
  }, (err) => {
    console.log(err)
    res.json(err)
  })
});

module.exports = router;
