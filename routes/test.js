"use strict"
var express = require('express');
var router = express.Router();
const Room = require('../models/Room')
const createUser = require('../helpers/userHelper')
/* GET users listing. */
router.get('/hello', function(req, res, next) {
  console.log(req.cookies)
  Room.find({}).exec().then(function (data) {
    console.log({rooms: data})
    res.json({rooms: data})
  }, (error) => {

    res.json({answer: error})
  })
});


router.post('/login', function(req, res, next) {

  let user = req.params
  createUser(user).then((user) => {
    console.log(user)
    res.json(user)
  }, (err) => {
    console.log(err)
    res.json(err)
  })
});

module.exports = router;
