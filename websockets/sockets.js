"use strict"
const WebSocketServer = require('ws').Server;
const cookie = require('cookie');
const computeSig = require('../helpers/authorization').computeSig;
const users = {};

function websockets(httpServer) {
  var wss = new WebSocketServer({server: httpServer, verifyClient: verifyClient});
  wss.on('connection', function connection(ws) {
    console.log('user connected')

    ws.on('message', function(message) {
      console.log(message);
    })

    const cookies = cookie.parse(ws.upgradeReq.headers.cookie)
    users[cookies.mid] = {
      socket: ws
    }
    console.log(users)
  });
}

function verifyClient(info) {
  let cookies = cookie.parse(info.req.headers.cookie);
  // console.log(cookies);
  const sig = computeSig(cookies.expire, cookies.mid, cookies.secret, cookies.sid);
  if(sig === cookies.sig)
    return true;
  return false;
}

module.exports = websockets;