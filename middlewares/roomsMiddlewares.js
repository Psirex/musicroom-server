"use strict"

const User = require('../models/User')
const mongoose = require('mongoose')
const errors = require('../errors/roomsErrors')

const checkRoomTitle = (req, res, next) => {
  const title = (req.body.title || "").trim()
  if (title) {
    next()
  } else {
    res.json(errors.titleIsMissing)
  }
}

const checkVkId = (req, res, next) => {
  const id = req.body.id
  if (id) {
    User.findOne({vkId: id}).then((user) => {
      if (user) {
        req.body.userId = user._id
        next()
      } else {
        res.json(errors.vkIdIsIncorrect)
      }
    })
  } else {
    res.json(errors.vkIdIsIncorrect)
  }
}

const checkObjectId = (req, res, next) => {
  console.log(req.params.id)
  if (mongoose.Types.ObjectId.isValid(req.params.id)) {
    next()
  } else {
    res.send(errors.incorrectObjectId)
  }
}

module.exports.checkRoomTitle = checkRoomTitle
module.exports.checkVkId = checkVkId
module.exports.checkObjectId = checkObjectId