const authorization = require('../helpers/authorization');
const isAuthorized = (req, res, next) => {
  const mid = req.cookies.mid
  const sid = req.cookies.sid
  const expire = req.cookies.expire
  const secret = req.cookies.secret
  const sig = authorization.computeSig(expire, mid, secret, sid)
  if(sig === req.cookies.sig){
    console.log(req.cookies)
    next();
  }
  else
    res.json({error: 'session data is not valid'})
}

module.exports.isAuthorized = isAuthorized;