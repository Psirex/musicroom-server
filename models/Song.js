const mongoose = require('mongoose')

const Schema = mongoose.Schema

const songSchema = new Schema({
  title: String,
  artist: String,
  duration: Number,
  vkId: String,
  ownerVkId: String,
  created_at: Date,
  updated_at: Date
})

const Song = mongoose.model('Song', songSchema)

module.exports = Song