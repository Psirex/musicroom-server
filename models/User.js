const mongoose = require('mongoose')
const findOrCreate = require('mongoose-findorcreate')
const Schema = mongoose.Schema

const UserSchema = new Schema({
  vkId: String,
  firstName: String,
  lastName: String,
  songs: [{type: Schema.Types.ObjectId, ref: 'Song'}],
  rooms: [{type: Schema.Types.ObjectId, ref: 'Room'}],
  created_at: Date,
  updated_at: Date
})
UserSchema.plugin(findOrCreate)

const User = mongoose.model('User', UserSchema)

module.exports = User