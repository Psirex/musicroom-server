const mongoose = require('mongoose')

const Schema = mongoose.Schema

const roomSchema = new Schema({
  title: String,
  currentSong: {type: Schema.Types.ObjectId, ref: 'Song'},
  owner: {type: Schema.Types.ObjectId, ref: 'User'},
  participations: [{type: Schema.Types.ObjectId, ref: 'User'}],
  songs: [{type: Schema.Types.ObjectId, ref: 'Song'}],
  created_at: Date,
  updated_at: Date
})

roomSchema.pre('save', (next) => {
  const currentDate = new Date()
  this.updated_at = currentDate
  if (!this.created_at)
    this.created_at = currentDate
  next()
})

const Room = mongoose.model('Room', roomSchema)

module.exports = Room