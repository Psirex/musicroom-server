"use strict"
const Room = require('../models/Room.js')

function testPromise(){
  let room = new Room({
    title: 'Test Room'
  })
  room.save().then(function(doc){
    console.log(doc)
  }, function(err){
    console.log(err)
  })
}

module.exports = testPromise