const User = require('../models/User')

var createUser = function (user) {
  return new Promise((resolve, reject) => {
    console.log(user)
    User.findOrCreate({vkId: user.id}, {firstName: user.firstName, lastName: user.lastName}, (err, user, created)=> {
      if (err) {
        reject(err)
      } else {
        resolve(user)
      }
    })
  })
}

module.exports = createUser