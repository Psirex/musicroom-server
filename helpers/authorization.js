const APP_SECRET_KEY = require('./constants').APP_SECRET_KEY;
const crypto = require('crypto');

const computeSig = (expire, mid, secret, sid) => {
  var concatedString = `expire=${expire}mid=${mid}secret=${secret}sid=${sid}${APP_SECRET_KEY}`;
  const crypted = crypto.createHash('md5').update(concatedString).digest("hex");
  return crypted;
}

module.exports.computeSig = computeSig;